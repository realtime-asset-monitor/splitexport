// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*
Package splitexport in the splitexport go module is a function based on the function framework to split cai export into smaller exports then intro cai asset feed like messages

# Triggered by

# A cloud event based on a storage event

# Output

- smaller export in the same bucket
- cai feed message like

# Cardinality

- one-many

# Automatic retrying

Yes.
*/
package splitexport
