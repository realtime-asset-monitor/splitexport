// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"context"
	"fmt"
	"io"
	"strings"

	"cloud.google.com/go/firestore"
	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/storage"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func processExport(ctx context.Context,
	gcsEvent storageEvent,
	storageClient *storage.Client,
	splitThresholdLineNumber int64,
	storageRangeReaderLengthBytes int64,
	firestoreClient *firestore.Client,
	exportCollectionName string,
	maxRetry int,
	topic *pubsub.Topic,
	ev glo.EntryValues) (finishMsg string,
	finishMsgDescription string,
	ratePerSecond float64,
	retry bool,
	err error) {
	// var buffer bytes.Buffer

	storageBucket := storageClient.Bucket(gcsEvent.Bucket)
	storageObject := storageBucket.Object(gcsEvent.Name)
	var offset, readCount, exportLineNumber int64

	for {
		var storageObjectRangeReader *storage.Reader
		storageObjectRangeReader, err = storageObject.NewRangeReader(ctx, offset, storageRangeReaderLengthBytes)
		if err != nil {
			break
		}
		var b []byte
		b, err = io.ReadAll(storageObjectRangeReader)
		if err != nil {
			break
		}
		s := string(b)
		lines := strings.SplitAfter(s, "\n")
		for _, line := range lines {
			if strings.Contains(line, "\n") {
				exportLineNumber++
			}
		}
		readCount++
		offset = offset + storageRangeReaderLengthBytes
	}
	if !strings.Contains(err.Error(), "The requested range cannot be satisfied") {
		return "", "", 0, true, fmt.Errorf("storageObject.NewRangeReader %s %s %v", storageObject.BucketName(), storageObject.ObjectName(), err)
	}
	err = nil
	// fmt.Printf("readCount %d exportLineNumber %d\n", readCount, exportLineNumber)
	if exportLineNumber > splitThresholdLineNumber {
		exportLineNumber, childExportNumber, duration, err := splitToChildExports(ctx,
			gcsEvent,
			storageClient,
			splitThresholdLineNumber,
			storageRangeReaderLengthBytes,
			firestoreClient,
			exportCollectionName,
			maxRetry,
			ev)
		finishMsg = "splitToChildExports done"
		finishMsgDescription = fmt.Sprintf("exportLineNumber %d childExportNumber %d duration %v minutes %s",
			exportLineNumber, childExportNumber, duration.Minutes(), gcsEvent.Name)
		ratePerSecond = float64(exportLineNumber) / duration.Seconds()
		return finishMsg, finishMsgDescription, ratePerSecond, false, err

	}

	exportLineNumber, pubSubMsgNumber, pubSubErrNumber, duration := splitToLines(ctx,
		gcsEvent,
		storageClient,
		storageRangeReaderLengthBytes,
		topic,
		ev.StepStack[0].StepTimestamp,
		ev)
	finishMsg = "splitToLines done"
	finishMsgDescription = fmt.Sprintf("exportLineNumber %d pubSubMsgNumber %d  pubSubErrNumber %d duration %v minutes %s",
		exportLineNumber, pubSubMsgNumber, pubSubErrNumber, duration.Minutes(), gcsEvent.Name)
	ratePerSecond = float64(exportLineNumber) / duration.Seconds()
	return finishMsg, finishMsgDescription, ratePerSecond, false, nil
}
