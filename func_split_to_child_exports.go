// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"cloud.google.com/go/firestore"
	"cloud.google.com/go/storage"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// 2022-03-26 removed with retry code as the client lib already does it
// https://pkg.go.dev/cloud.google.com/go/storage@v1.21.0#hdr-Retrying_failed_requests

func splitToChildExports(ctx context.Context,
	gcsEvent storageEvent,
	storageClient *storage.Client,
	splitThresholdLineNumber int64,
	storageRangeReaderLengthBytes int64,
	firestoreClient *firestore.Client,
	exportCollectionName string,
	maxRetry int,
	ev glo.EntryValues) (exportLineNumber int64,
	childExportNumber int64,
	duration time.Duration,
	err error) {
	start := time.Now()
	storageBucket := storageClient.Bucket(gcsEvent.Bucket)
	storageObjectFrom := storageBucket.Object(gcsEvent.Name)

	childExportName := fmt.Sprintf("%s.%s.%s.child%d",
		gcsEvent.Name,
		gcsEvent.Generation,
		strings.Replace(gcsEvent.Updated.Format(time.RFC3339), ":", "_", -1),
		childExportNumber)

	storageObjectTo := storageBucket.Object(childExportName)
	storageObjectWriter := storageObjectTo.NewWriter(ctx)

	var offset, childExportLineNumber int64
	var assetContent, childExportContent string

	for {
		var storageObjectRangeReader *storage.Reader
		storageObjectRangeReader, err = storageObjectFrom.NewRangeReader(ctx, offset, storageRangeReaderLengthBytes)
		if err != nil {
			break
		}
		var b []byte
		b, err = io.ReadAll(storageObjectRangeReader)
		if err != nil {
			break
		}
		s := string(b)

		lines := strings.SplitAfter(s, "\n")
		for _, line := range lines {
			assetContent = assetContent + line
			if strings.Contains(line, "\n") {
				if childExportLineNumber < splitThresholdLineNumber {
					childExportContent = childExportContent + assetContent
				} else {
					_, err := writeAndRecord(storageObjectWriter, childExportContent, childExportName, storageObjectTo, firestoreClient,
						exportCollectionName, global.serviceEnv.MaxRetry, ev)
					if err != nil {
						return exportLineNumber, childExportNumber, time.Since(start), err
					}
					childExportNumber++
					childExportLineNumber = 0
					childExportName = fmt.Sprintf("%s.%s.%s.child%d",
						gcsEvent.Name,
						gcsEvent.Generation,
						strings.Replace(gcsEvent.Updated.Format(time.RFC3339), ":", "_", -1),
						childExportNumber)
					storageObjectTo = storageBucket.Object(childExportName)
					storageObjectWriter = storageObjectTo.NewWriter(ctx)
					childExportContent = assetContent
				}
				exportLineNumber++
				childExportLineNumber++
				assetContent = ""
			}
		}
		offset = offset + storageRangeReaderLengthBytes
	}
	if !strings.Contains(err.Error(), "The requested range cannot be satisfied") {
		return exportLineNumber, childExportNumber, time.Since(start), err
	}

	_, err = writeAndRecord(storageObjectWriter, childExportContent, childExportName, storageObjectTo, firestoreClient,
		exportCollectionName, global.serviceEnv.MaxRetry, ev)
	if err != nil {
		return exportLineNumber, childExportNumber, time.Since(start), err
	}
	return exportLineNumber, childExportNumber, time.Since(start), nil
}

func writeAndRecord(writer *storage.Writer,
	content string,
	childExportName string,
	storageObject *storage.ObjectHandle,
	firestoreClient *firestore.Client,
	exportCollectionName string,
	maxRetry int,
	ev glo.EntryValues) (objAttrs *storage.ObjectAttrs, err error) {
	_, err = fmt.Fprint(writer, content)
	if err != nil {
		return objAttrs, fmt.Errorf("fmt.Fprint(writer, content) %v", err)
	}
	err = writer.Close()
	if err != nil {
		return objAttrs, fmt.Errorf("writer.Close() %v", err)
	}

	err = gfs.RecordExportWithRetry(ctx,
		firestoreClient,
		exportCollectionName,
		childExportName,
		false,
		maxRetry,
		ev)
	if err != nil {
		glo.LogWarning(ev, "unable to persist the child export stepstack to firestore", fmt.Sprintf("%s %v", childExportName, err))
	}
	return objAttrs, nil
}
