// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"context"
	"fmt"
	"strings"

	"cloud.google.com/go/firestore"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func getExportRecord(ctx context.Context, exportName string,
	firestoreClient *firestore.Client,
	exportCollectionName string,
	maxRetry int,
	ev glo.EntryValues) (exportRecord gfs.ExportRecord, err error) {

	// 1 look for the exportRecord of a GCS object
	docName := exportName
	documentPath := fmt.Sprintf("%s/%s", exportCollectionName, strings.ReplaceAll(docName, "/", "\\"))
	if !strings.Contains(exportName, ".child") {
		maxRetry = 1
	}
	docSnapShot, found := gfs.GetDocWithRetry(ctx, firestoreClient, documentPath, maxRetry, true, ev)
	if found {
		err = docSnapShot.DataTo(&exportRecord)
		if err != nil {
			return exportRecord, err
		}
		return exportRecord, nil
	}
	if strings.Contains(exportName, ".child") {
		return exportRecord, fmt.Errorf("doc not found in firestore %s", documentPath)
	}

	// 2 if not found look for the exportRecord of the triggering CAI export command
	parts := strings.Split(exportName, "/")
	docName = parts[0]
	documentPath = fmt.Sprintf("%s/%s", exportCollectionName, strings.ReplaceAll(docName, "/", "\\"))
	docSnapShot, found = gfs.GetDocWithRetry(ctx, firestoreClient, documentPath, maxRetry, true, ev)
	if !found {
		return exportRecord, fmt.Errorf("doc not found in firestore %s", documentPath)
	}
	err = docSnapShot.DataTo(&exportRecord)
	if err != nil {
		return exportRecord, err
	}
	return exportRecord, nil
}
