// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegEntryPoint(t *testing.T) {
	testCases := []struct {
		name            string
		path            string
		eventType       string
		notifType       string
		processed       bool
		wantMsgContains []string
		wantRetry       bool
	}{
		{
			name:      "ok",
			path:      "testdata/gcsevent02.json",
			eventType: "OBJECT_FINALIZE",
			notifType: "caiexport",
			wantMsgContains: []string{
				"start cloudevent",
				"exportLineNumber 7 pubSubMsgNumber 7  pubSubErrNumber 0",
			},
		},
		{
			name:      "duplicate",
			path:      "testdata/gcsevent02.json",
			eventType: "OBJECT_FINALIZE",
			notifType: "caiexport",
			processed: true,
			wantMsgContains: []string{
				"finish cancel",
				"ignored duplicate event",
			},
		},
		{
			name:      "not_ndjson",
			path:      "testdata/gcsevent01.json",
			eventType: "OBJECT_FINALIZE",
			notifType: "caiexport",
			wantMsgContains: []string{
				"ignored contentType is not new line delimited JSON",
			},
		},
		{
			name:      "deleted_object",
			path:      "testdata/gcsevent03.json",
			eventType: "OBJECT_FINALIZE",
			notifType: "caiexport",
			wantMsgContains: []string{
				"deleted object",
			},
		},
		{
			name:      "empty_object",
			path:      "testdata/gcsevent04.json",
			eventType: "OBJECT_FINALIZE",
			notifType: "caiexport",
			wantMsgContains: []string{
				"empty object",
			},
		},
		{
			name:      "wrong_event_type",
			path:      "testdata/gcsevent04.json",
			eventType: "blabla",
			notifType: "caiexport",
			wantMsgContains: []string{
				"want eventType OBJECT_FINALIZE and got",
			},
		},
		{
			name:      "wrong_notif_type",
			path:      "testdata/gcsevent04.json",
			eventType: "OBJECT_FINALIZE",
			notifType: "blabla",
			wantMsgContains: []string{
				"want notif-type caiexport and got",
			},
		},
		{
			name:      "gcsEvent_wrong_format",
			path:      "testdata/gcsevent05.json",
			eventType: "OBJECT_FINALIZE",
			notifType: "caiexport",
			wantMsgContains: []string{
				"invalid character",
			},
		},
		{
			name:      "not_composite_object",
			path:      "testdata/gcsevent06.json",
			eventType: "OBJECT_FINALIZE",
			notifType: "caiexport",
			wantMsgContains: []string{
				"ignored as is not the final composite object",
			},
		},
		{
			name:      "retry_on_storage_error",
			path:      "testdata/gcsevent07.json",
			eventType: "OBJECT_FINALIZE",
			notifType: "caiexport",
			wantRetry: true,
		},
	}
	ctx := context.Background()

	var ev glo.EntryValues
	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "test"
	ev.Step.StepID = "0123456789"

	now := time.Now()
	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			p := filepath.Clean(tc.path)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			attr := make(map[string]string)
			attr["eventTime"] = now.Format(time.RFC3339)
			attr["eventType"] = tc.eventType
			attr["notif-type"] = tc.notifType

			var pubsubMsg pubsub.Message
			pubsubMsg.Attributes = attr
			pubsubMsg.ID = "c56c9245-0af7-44c7-8a55-954583940e09"
			pubsubMsg.PublishTime = now
			pubsubMsg.Data = b

			if tc.processed {
				var storageEvent storageEvent
				err := json.Unmarshal(b, &storageEvent)
				if err != nil {
					t.Error(err)
				}
				err = gfs.RecordExportWithRetry(ctx,
					global.firestoreClient,
					global.serviceEnv.ExportCollectionName,
					storageEvent.Name,
					tc.processed,
					10,
					ev)
				if err != nil {
					t.Error(err)
				}
			}

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			err = EntryPoint(ctx, pubsubMsg)
			msgString := buffer.String()
			// t.Logf("%v", msgString)

			gfs.DeleteCollection(ctx,
				global.firestoreClient,
				global.firestoreClient.Collection(global.serviceEnv.ExportCollectionName),
				100)
			if err != nil {
				if !tc.wantRetry {
					t.Errorf("want no retry got one as exit with error %v", err)
				}
			} else {
				if tc.wantRetry {
					t.Errorf("want retry and got no error to trigger it")
				}
			}
			for _, wantMsg := range tc.wantMsgContains {
				if !strings.Contains(msgString, wantMsg) {
					t.Errorf("want msg to contains: %s", tc.wantMsgContains)
				}
			}
			logEntriesString := strings.Split(msgString, "\n")
			for _, logEntryString := range logEntriesString {
				var logEntry glo.Entry
				err = json.Unmarshal([]byte(logEntryString), &logEntry)
				if !(err != nil) {
					if logEntry.MicroserviceName == "" {
						t.Errorf("logEntry.MicroserviceName is null should not %s", logEntry.Message)
					}
					if logEntry.Environment == "" {
						t.Errorf("logEntry.Environment is null should not %s", logEntry.Message)
					}
					if strings.HasPrefix(strings.ToLower(logEntry.Message), "finish") {
						if len(logEntry.StepStack) == 0 {
							t.Errorf("logEntry.StepStack len is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencyE2ESeconds == 0 {
							t.Errorf("logEntry.LatencyE2ESeconds is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencySeconds == 0 {
							t.Errorf("logEntry.LatencySeconds is zero and should not %s", logEntry.Message)
						}
					}
				}
			}
		})
	}
}
