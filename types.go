// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package splitexport

import (
	"time"

	"cloud.google.com/go/firestore"
	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/storage"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

const microserviceName = "splitexport"

// ServiceEnv list environment variables prefixed with the name of the microservice
type ServiceEnv struct {
	CAIFeedTopicID                string `envconfig:"cai_feed_topic_id" default:"caiFeed"`
	Environment                   string `envconfig:"environment" default:"dev"`
	ExportCollectionName          string `envconfig:"export_collection_name" default:"exports"`
	LogOnlySeveritylevels         string `envconfig:"log_only_severity_levels" default:"WARNING NOTICE CRITICAL"`
	MaxRetry                      int    `envconfig:"max_retry" default:"10"`
	ProjectID                     string `envconfig:"project_id" required:"true"`
	SplitThresholdLineNumber      int64  `envconfig:"split_threshold_line_number" default:"1000"`
	StartProfiler                 bool   `envconfig:"start_profiler" default:"false"`
	StorageRangeReaderLengthBytes int64  `envconfig:"storage_range_reader_length_bytes" default:"10485760"`
}

// Env list environment variables
type Env struct {
	KConfiguration string `envconfig:"k_configuration"`
	KRevision      string `envconfig:"k_revision"`
	KService       string `envconfig:"k_service"`
}

// Global structure for global variables to optimize the performances in serverless mode
type Global struct {
	env             *Env
	firestoreClient *firestore.Client
	serviceEnv      *ServiceEnv
	CommonEv        glo.CommonEntryValues
	pubsubTopic     *pubsub.Topic
	storageClient   *storage.Client
}

// storageEvent is the payload of a GCS event.
type storageEvent struct {
	Bucket             string `json:"bucket"`
	CacheControl       string `json:"cacheControl"`
	ComponentCount     int    `json:"componentCount"`
	ContentDisposition string `json:"contentDisposition"`
	ContentEncoding    string `json:"contentEncoding"`
	ContentType        string `json:"contentType"`
	CRC32C             string `json:"crc32c"`
	CustomerEncryption struct {
		EncryptionAlgorithm string `json:"encryptionAlgorithm"`
		KeySha256           string `json:"keySha256"`
	}
	Etag                    string                 `json:"etag"`
	EventBasedHold          bool                   `json:"eventBasedHold"`
	Generation              string                 `json:"generation"`
	ID                      string                 `json:"id"`
	Kind                    string                 `json:"kind"`
	KMSKeyName              string                 `json:"kmsKeyName"`
	MD5Hash                 string                 `json:"md5Hash"`
	MediaLink               string                 `json:"mediaLink"`
	Metadata                map[string]interface{} `json:"metadata"`
	Metageneration          string                 `json:"metageneration"`
	Name                    string                 `json:"name"`
	ResourceState           string                 `json:"resourceState"`
	RetentionExpirationTime time.Time              `json:"retentionExpirationTime"`
	SelfLink                string                 `json:"selfLink"`
	Size                    string                 `json:"size"`
	StorageClass            string                 `json:"storageClass"`
	TemporaryHold           bool                   `json:"temporaryHold"`
	TimeCreated             time.Time              `json:"timeCreated"`
	TimeStorageClassUpdated time.Time              `json:"timeStorageClassUpdated"`
	Updated                 time.Time              `json:"updated"`
}
