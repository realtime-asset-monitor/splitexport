// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegSplitToChildExports(t *testing.T) {
	testCases := []struct {
		name                          string
		bucketNameSuffix              string
		objectName                    string
		splitThresholdLineNumber      int64
		storageRangeReaderLengthBytes int64
		wantErrMsgContains            string
		wantExportLineNumber          int64
		wantChildExportNumber         int64
	}{
		{
			name:                          "iam_export_64B_ok",
			bucketNameSuffix:              "testexport",
			objectName:                    "t2022-03-14T07-34-17-275598699Z_fld800047992631_iam_policy_7929110e74d513b78cb7b24b8f9cdc7f2bbe5f3f344eec5027a819de0f076013/cloudresourcemanager.googleapis.com/Folder/0",
			splitThresholdLineNumber:      2,
			storageRangeReaderLengthBytes: 64,
			wantExportLineNumber:          19,
			wantChildExportNumber:         9,
		},
		{
			name:                          "iam_export_1MiB_ok",
			bucketNameSuffix:              "testexport",
			objectName:                    "t2022-03-14T07-34-17-275598699Z_fld800047992631_iam_policy_7929110e74d513b78cb7b24b8f9cdc7f2bbe5f3f344eec5027a819de0f076013/cloudresourcemanager.googleapis.com/Folder/0",
			splitThresholdLineNumber:      2,
			storageRangeReaderLengthBytes: 1048576,
			wantExportLineNumber:          19,
			wantChildExportNumber:         9,
		},
		{
			name:                          "storage_error",
			bucketNameSuffix:              "blabla",
			objectName:                    "blabla",
			splitThresholdLineNumber:      2,
			storageRangeReaderLengthBytes: 1048576,
			wantErrMsgContains:            "storage: object doesn't exist",
		},
	}
	var ev glo.EntryValues
	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "splitexport"
	ev.Step.StepID = "0123456789"

	ctx := context.Background()
	exportCollectionName := "TestIntegSplitToChildExports"

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			var gcsEvent storageEvent
			gcsEvent.Bucket = fmt.Sprintf("%s-%s", global.serviceEnv.ProjectID, tc.bucketNameSuffix)
			gcsEvent.Name = tc.objectName

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()

			exportLineNumber, childExportNumber, duration, err := splitToChildExports(ctx,
				gcsEvent,
				global.storageClient,
				tc.splitThresholdLineNumber,
				tc.storageRangeReaderLengthBytes,
				global.firestoreClient,
				exportCollectionName,
				global.serviceEnv.MaxRetry,
				ev)
			msgString := buffer.String()
			t.Logf("%v", msgString)
			// t.Logf("exportLineNumber %d childExportNumber %d duration %v", exportLineNumber, childExportNumber, duration)

			if duration == 0 {
				t.Errorf("Duration is zero and should not")
			}

			if err != nil {
				if !strings.Contains(err.Error(), tc.wantErrMsgContains) {
					t.Errorf("want error msg contains %s and got %s", tc.wantErrMsgContains, err.Error())
				}
			} else {
				if tc.wantErrMsgContains != "" {
					t.Errorf("want error msg contains %s and got no error", tc.wantErrMsgContains)
				}
				if exportLineNumber != tc.wantExportLineNumber {
					t.Errorf("want exportLineNumber equals %d and got %d", tc.wantExportLineNumber, exportLineNumber)
				}
				if childExportNumber != tc.wantChildExportNumber {
					t.Errorf("want childExportNumber equals %d and got %d", tc.wantChildExportNumber, childExportNumber)
				}
			}
		})
	}
	err := gfs.DeleteCollection(ctx,
		global.firestoreClient,
		global.firestoreClient.Collection(exportCollectionName),
		100)
	if err != nil {
		log.Fatalln(err)
	}
}
