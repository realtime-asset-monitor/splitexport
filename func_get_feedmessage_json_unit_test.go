// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestUnitGetFeedMessageJSON(t *testing.T) {
	testCases := []struct {
		name               string
		path               string
		wantErrMsgContains string
	}{
		{
			name: "iam_project_ok",
			path: "testdata/dumpline_01.txt",
		},
		{
			name: "resource_pod_ok",
			path: "testdata/dumpline_02.txt",
		},
		{
			name:               "dumpline_invalid_character",
			path:               "testdata/dumpline_03.txt",
			wantErrMsgContains: "invalid character",
		},
	}
	var ev glo.EntryValues
	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "splitexport"
	ev.Step.StepID = "0123456789"
	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			p := filepath.Clean(tc.path)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			now := time.Now()
			r, err := getFeedMessageJSON(string(b), now, ev)
			// t.Logf("%s\n", r)

			if err != nil {
				if tc.wantErrMsgContains != "" {
					if !strings.Contains(err.Error(), tc.wantErrMsgContains) {
						t.Errorf("want error msg contains %s and got %s", tc.wantErrMsgContains, err.Error())
					}
				} else {
					t.Errorf("Got unexpected error %v", err)
				}
			} else {
				var feedMsg cai.FeedMessage
				err := json.Unmarshal(r, &feedMsg)
				if err != nil {
					t.Errorf("Unable to unmarshal returned data into a feed message %s", err.Error())
				} else {
					if feedMsg.Asset.AssetType != feedMsg.Asset.AssetTypeLegacy {
						t.Errorf("feedMsg.Asset.AssetType does not equal feedMsg.Asset.AssetTypeLegacy")
					}
					if len(feedMsg.Asset.IamPolicyLegacy) != len(feedMsg.Asset.IamPolicy) {
						t.Errorf("feedMsg.Asset.IamPolicyLegacy length does not equal feedMsg.Asset.IamPolicy length")
					}
					if feedMsg.Window.StartTime.Sub(now).Seconds() != 0 {
						t.Errorf("Difference %v between feedMsg.Window.StartTime %v and now %v is more than 0 seconds", feedMsg.Window.StartTime.Sub(now).Seconds(), feedMsg.Window.StartTime, now)
					}
					if feedMsg.Origin != "scheduled" {
						t.Errorf("feedMsg.Origin is not 'scheduled'")
					}
					if feedMsg.Deleted {
						t.Errorf("want feedMsg.Deleted false and got true")
					}
				}
			}
		})
	}
}
