// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// EntryPoint function entry point.
func EntryPoint(ctxEvent context.Context, pubsubMsg pubsub.Message) error {
	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv
	publishTime, err := time.Parse(time.RFC3339, pubsubMsg.Attributes["eventTime"])
	if err != nil {
		glo.LogWarning(ev, "pubsub msg publish time missing or invalid", fmt.Sprintf("time.Parse(time.RFC3339,  pubsubMsg.Attributes[\"eventTime\"]) %v", err))
	}
	ev.Step = glo.Step{
		StepID:        fmt.Sprintf("gcs_notif_to_pubsub_msg/%s", pubsubMsg.ID),
		StepTimestamp: publishTime,
	}
	ev.StepStack = make(glo.Steps, 0)
	ev.StepStack = append(ev.StepStack, ev.Step)
	now := time.Now()
	d := now.Sub(ev.Step.StepTimestamp)

	b, _ := json.MarshalIndent(pubsubMsg, "", "  ")
	glo.LogStartCloudEvent(ev, b, d.Seconds(), &now)

	b, _ = json.MarshalIndent(pubsubMsg.Attributes, "", "  ")
	glo.LogInfo(ev, "pubsub msg attributes", string(b))

	if pubsubMsg.Attributes["eventType"] != "OBJECT_FINALIZE" {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("want eventType OBJECT_FINALIZE and got %s", pubsubMsg.Attributes["eventType"]), "", "")
		return nil
	}

	if pubsubMsg.Attributes["notif-type"] != "caiexport" {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("want notif-type caiexport and got %s", pubsubMsg.Attributes["notif-type"]), "", "")
		return nil
	}

	var gcsEvent storageEvent
	err = json.Unmarshal(pubsubMsg.Data, &gcsEvent)
	if err != nil {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("cloudEvent.DataAs(&gcsEvent) %v", err), "", "")
		return nil
	}

	var finishMsg, finishMsgDescription string
	var ratePerSecond float64
	ignored := false
	if gcsEvent.ResourceState == "not_exists" {
		finishMsg = "cancel"
		finishMsgDescription = fmt.Sprintf("deleted object %v", gcsEvent.Name)
		ignored = true
	} else {
		if gcsEvent.Size == "0" {
			finishMsg = "cancel"
			finishMsgDescription = fmt.Sprintf("empty object %v", gcsEvent.Name)
			ignored = true
		} else {
			if !strings.Contains(gcsEvent.Name, ".child") {
				if gcsEvent.ContentType != "application/x-ndjson" {
					finishMsg = "cancel"
					finishMsgDescription = fmt.Sprintf("ignored contentType is not new line delimited JSON %s", gcsEvent.Name)
					ignored = true
				} else {
					// cai export using uriPrefix are delivered in shards.
					// Each shard is a GCS eventually a composite object.
					// Let's assume then non composite object can be ignored to avoid duplicate work and wasting resources
					// gcs composite object have a componentCount > 0 and no md5Hash
					if gcsEvent.ComponentCount == 0 {
						finishMsg = "cancel"
						finishMsgDescription = fmt.Sprintf("ignored as is not the final composite object %s", gcsEvent.Name)
						ignored = true
					}
				}
			}
		}
	}
	if !ignored {
		exportRecord, err := getExportRecord(ctx,
			gcsEvent.Name,
			global.firestoreClient,
			global.serviceEnv.ExportCollectionName,
			global.serviceEnv.MaxRetry,
			ev)
		if err != nil {
			// ignore logged warning are enough
			_ = err
		}
		if exportRecord.Processed {
			finishMsg = "cancel"
			finishMsgDescription = fmt.Sprintf("ignored duplicate event %s", gcsEvent.Name)
		} else {
			err := gfs.RecordExportWithRetry(ctxEvent,
				global.firestoreClient,
				global.serviceEnv.ExportCollectionName,
				gcsEvent.Name,
				true,
				global.serviceEnv.MaxRetry,
				ev)
			if err != nil {
				// ignore logged warning are enough
				_ = err
			}
			if exportRecord.StepStack != nil {
				ev.StepStack = exportRecord.StepStack
			} else {
				ev.StepStack = make(glo.Steps, 0)
			}
			var gcsStep glo.Step
			gcsStep.StepTimestamp = gcsEvent.Updated
			gcsStep.StepID = gcsEvent.ID
			ev.StepStack = append(ev.StepStack, gcsStep)
			ev.StepStack = append(ev.StepStack, ev.Step)

			var retry bool
			finishMsg, finishMsgDescription, ratePerSecond, retry, err = processExport(ctxEvent,
				gcsEvent,
				global.storageClient,
				global.serviceEnv.SplitThresholdLineNumber,
				global.serviceEnv.StorageRangeReaderLengthBytes,
				global.firestoreClient,
				global.serviceEnv.ExportCollectionName,
				global.serviceEnv.MaxRetry,
				global.pubsubTopic,
				ev)
			if err != nil {
				if retry {
					glo.LogCriticalRetry(ev, err.Error(), "", "")
					err2 := gfs.RecordExportWithRetry(ctxEvent,
						global.firestoreClient,
						global.serviceEnv.ExportCollectionName,
						gcsEvent.Name,
						false,
						global.serviceEnv.MaxRetry,
						ev)
					if err2 != nil {
						// ignore logged warning are enough
						_ = err
					}
					return err
				}
				glo.LogCriticalNoRetry(ev, err.Error(), "", "")
				return nil
			}
		}
	}

	gcsEventJSON, _ := json.MarshalIndent(&gcsEvent, "", "    ")
	finishMsgDescription = fmt.Sprintf("%s %s", finishMsgDescription, string(gcsEventJSON))
	glo.LogFinish(ev, finishMsg, finishMsgDescription, time.Now(), "scheduled", "", "", "", ratePerSecond)
	return nil
}
