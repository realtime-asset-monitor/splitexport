// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"context"
	"fmt"
	"log"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegProcessExport(t *testing.T) {
	testCases := []struct {
		name                             string
		bucketNameSuffix                 string
		objectName                       string
		splitThresholdLineNumber         int64
		storageRangeReaderLengthBytes    int64
		wantRetry                        bool
		wantErrMsgContains               string
		wantFinishMsgDescriptionContains string
	}{
		{
			name:                             "iam_export_1MiB_ok",
			bucketNameSuffix:                 "testexport",
			objectName:                       "export_iam.txt",
			splitThresholdLineNumber:         1000,
			storageRangeReaderLengthBytes:    1048576,
			wantFinishMsgDescriptionContains: "exportLineNumber 7 pubSubMsgNumber 7  pubSubErrNumber 0",
		},
		{
			name:                             "iam_export_64B_ok",
			bucketNameSuffix:                 "testexport",
			objectName:                       "export_iam.txt",
			splitThresholdLineNumber:         1000,
			storageRangeReaderLengthBytes:    64,
			wantFinishMsgDescriptionContains: "exportLineNumber 7 pubSubMsgNumber 7  pubSubErrNumber 0",
		},
		{
			name:                             "pod_export_1MiB_ok",
			bucketNameSuffix:                 "testexport",
			objectName:                       "export_pod.txt",
			splitThresholdLineNumber:         1000,
			storageRangeReaderLengthBytes:    1048576,
			wantFinishMsgDescriptionContains: "exportLineNumber 28 pubSubMsgNumber 28  pubSubErrNumber 0",
		},
		{
			name:                             "pod_export_2KB_ok",
			bucketNameSuffix:                 "testexport",
			objectName:                       "export_pod.txt",
			splitThresholdLineNumber:         1000,
			storageRangeReaderLengthBytes:    2048,
			wantFinishMsgDescriptionContains: "exportLineNumber 28 pubSubMsgNumber 28  pubSubErrNumber 0",
		},
		{
			name:                             "iam_export_to_child_1Mib",
			bucketNameSuffix:                 "testexport",
			objectName:                       "export_iam.txt",
			splitThresholdLineNumber:         2,
			storageRangeReaderLengthBytes:    1048576,
			wantFinishMsgDescriptionContains: "exportLineNumber 7 childExportNumber 3",
		},
		{
			name:                             "iam_export_to_child_64B",
			bucketNameSuffix:                 "testexport",
			objectName:                       "export_iam.txt",
			splitThresholdLineNumber:         2,
			storageRangeReaderLengthBytes:    64,
			wantFinishMsgDescriptionContains: "exportLineNumber 7 childExportNumber 3",
		},
		{
			name:                             "pod_export_to_child_1Mib",
			bucketNameSuffix:                 "testexport",
			objectName:                       "export_pod.txt",
			splitThresholdLineNumber:         2,
			storageRangeReaderLengthBytes:    1048576,
			wantFinishMsgDescriptionContains: "exportLineNumber 28 childExportNumber 13",
		},
		{
			name:                          "obj_does_not_exist",
			bucketNameSuffix:              "blabla",
			objectName:                    "blabla",
			storageRangeReaderLengthBytes: 1048576,
			wantRetry:                     true,
			wantErrMsgContains:            "object doesn't exist",
		},
	}
	var ev glo.EntryValues
	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "splitexport"
	ev.Step.StepID = "0123456789"
	ev.Step.StepTimestamp = time.Now()
	ev.StepStack = make(glo.Steps, 0)
	ev.StepStack = append(ev.StepStack, ev.Step)
	ctx := context.Background()
	pubSubClient, err := pubsub.NewClient(ctx, global.serviceEnv.ProjectID)
	if err != nil {
		t.Errorf("pubsub.NewClient %v", err)
	}
	topic := pubSubClient.Topic("cev-publish")
	exportCollectionName := "TestIntegProcessExport"

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			var gcsEvent storageEvent
			gcsEvent.Bucket = fmt.Sprintf("%s-%s", global.serviceEnv.ProjectID, tc.bucketNameSuffix)
			gcsEvent.Name = tc.objectName
			finishMsg, finishMsgDescription, ratePerSecond, retry, err := processExport(ctx,
				gcsEvent,
				global.storageClient,
				tc.splitThresholdLineNumber,
				tc.storageRangeReaderLengthBytes,
				global.firestoreClient,
				exportCollectionName,
				global.serviceEnv.MaxRetry,
				topic,
				ev)
			// t.Logf("%s %s %v %v", finishMsg, finishMsgDescription, retry, err)
			if retry != tc.wantRetry {
				t.Errorf("want retry %v and got %v", tc.wantRetry, retry)
			}
			if err != nil {
				if tc.wantErrMsgContains == "" {
					t.Errorf("Want no error and got %v", err)
				} else {
					if !strings.Contains(err.Error(), tc.wantErrMsgContains) {
						t.Errorf("Want error msg contains %s and got %s", tc.wantErrMsgContains, err.Error())
					}
				}
			} else {
				if !strings.Contains(finishMsg, "done") {
					t.Errorf("finishMsg does NOT contains 'done' and it should")
				}
				if !strings.Contains(finishMsgDescription, tc.wantFinishMsgDescriptionContains) {
					t.Errorf("Want finishMsgDescription contains %s and got %s", tc.wantFinishMsgDescriptionContains, finishMsgDescription)
				}
				if ratePerSecond == 0 {
					t.Errorf("ratePerSecond is 0 and it should not be")
				}
			}
		})
	}
	err = gfs.DeleteCollection(ctx,
		global.firestoreClient,
		global.firestoreClient.Collection(exportCollectionName),
		100)
	if err != nil {
		log.Fatalln(err)
	}
}
