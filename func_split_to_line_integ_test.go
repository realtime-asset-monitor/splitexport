// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegSplitToLine(t *testing.T) {
	testCases := []struct {
		name                          string
		bucketNameSuffix              string
		objectName                    string
		storageRangeReaderLengthBytes int64
		wantMsgContains               []string
		dumpLineNumber                int64
		pubSubMsgNumber               uint64
		pubSubErrNumber               uint64
	}{
		{
			name:                          "iam_export_64B_ok",
			bucketNameSuffix:              "testexport",
			objectName:                    "export_iam.txt",
			storageRangeReaderLengthBytes: 64,
			dumpLineNumber:                7,
			pubSubMsgNumber:               7,
			pubSubErrNumber:               0,
		},
		{
			name:                          "iam_export_1MiB_ok",
			bucketNameSuffix:              "testexport",
			objectName:                    "export_iam.txt",
			storageRangeReaderLengthBytes: 1048576,
			dumpLineNumber:                7,
			pubSubMsgNumber:               7,
			pubSubErrNumber:               0,
		},
		{
			name:                          "pod_export_2KB_ok",
			bucketNameSuffix:              "testexport",
			objectName:                    "export_pod.txt",
			storageRangeReaderLengthBytes: 2048,
			dumpLineNumber:                28,
			pubSubMsgNumber:               28,
			pubSubErrNumber:               0,
		},
		{
			name:                          "pod_export_1MiB_ok",
			bucketNameSuffix:              "testexport",
			objectName:                    "export_pod.txt",
			storageRangeReaderLengthBytes: 1048576,
			dumpLineNumber:                28,
			pubSubMsgNumber:               28,
			pubSubErrNumber:               0,
		},
		{
			name:                          "iam_export_bug",
			bucketNameSuffix:              "testexport",
			objectName:                    "export_iam_bug.txt",
			storageRangeReaderLengthBytes: 1048576,
			wantMsgContains: []string{
				"invalid character",
			},
			dumpLineNumber:  7,
			pubSubMsgNumber: 6,
			pubSubErrNumber: 0,
		},
	}
	var ev glo.EntryValues
	projectID := os.Getenv("SPLITEXPORT_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var SPLITEXPORT_PROJECT_ID")
	}
	ctx := context.Background()
	pubSubClient, err := pubsub.NewClient(ctx, projectID)
	if err != nil {
		t.Errorf("pubsub.NewClient %v", err)
	}
	topic := pubSubClient.Topic("cev-publish")

	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "splitexport"
	ev.Step.StepID = "0123456789"

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			var gcsEvent storageEvent
			gcsEvent.Bucket = fmt.Sprintf("%s-%s", global.serviceEnv.ProjectID, tc.bucketNameSuffix)
			gcsEvent.Name = tc.objectName

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()

			dumpLineNumber, pubSubMsgNumber, pubSubErrNumber, duration := splitToLines(ctx,
				gcsEvent,
				global.storageClient,
				tc.storageRangeReaderLengthBytes,
				topic,
				time.Now(),
				ev)
			msgString := buffer.String()
			// t.Logf("%v", msgString)
			// t.Logf("%d %d %d %v", dumpLineNumber, pubSubMsgNumber, pubSubErrNumber, duration)

			if duration == 0 {
				t.Errorf("Duration is zero and should not")
			}
			if len(tc.wantMsgContains) == 0 {
				if dumpLineNumber != int64(pubSubMsgNumber) {
					t.Errorf("dumpLineNumber is %d and is not equal to pubSubMsgNumber that is %d", dumpLineNumber, pubSubMsgNumber)
				}
				if pubSubErrNumber != 0 {
					t.Errorf("pubSubMsgNumber is %d and should be zero", pubSubMsgNumber)
				}
			} else {
				for _, wantMsg := range tc.wantMsgContains {
					wantMsg := wantMsg
					if !strings.Contains(msgString, wantMsg) {
						t.Errorf("want msg to contains: %s", tc.wantMsgContains)
					}
				}
				if dumpLineNumber != tc.dumpLineNumber {
					t.Errorf("Want dumpLineNumber equal %d and got %d", tc.dumpLineNumber, dumpLineNumber)
				}
				if pubSubMsgNumber != tc.pubSubMsgNumber {
					t.Errorf("Want pubSubMsgNumber equal %d and got %d", tc.pubSubMsgNumber, pubSubMsgNumber)
				}
				if pubSubErrNumber != tc.pubSubErrNumber {
					t.Errorf("Want pubSubErrNumber equal %d and got %d", tc.pubSubErrNumber, pubSubErrNumber)
				}
			}
		})
	}
}
