// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"fmt"
	"io"
	"strings"
	"sync"
	"time"

	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/storage"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"gitlab.com/realtime-asset-monitor/utilities/gps"
	"golang.org/x/net/context"
)

func splitToLines(ctx context.Context,
	gcsEvent storageEvent,
	storageClient *storage.Client,
	storageRangeReaderLengthBytes int64,
	topic *pubsub.Topic,
	startTime time.Time,
	ev glo.EntryValues) (dumpLineNumber int64,
	pubSubMsgNumber uint64,
	pubSubErrNumber uint64,
	duration time.Duration) {
	var waitGroup sync.WaitGroup
	var offset int64
	var err error
	var assetContent string

	storageBucket := storageClient.Bucket(gcsEvent.Bucket)
	storageObject := storageBucket.Object(gcsEvent.Name)

	dumpLineNumber = 0
	pubSubErrNumber = 0
	pubSubMsgNumber = 0
	start := time.Now()

	for {
		var storageObjectRangeReader *storage.Reader
		storageObjectRangeReader, err = storageObject.NewRangeReader(ctx, offset, storageRangeReaderLengthBytes)
		if err != nil {
			break
		}
		var b []byte
		b, err = io.ReadAll(storageObjectRangeReader)
		if err != nil {
			break
		}
		s := string(b)
		lines := strings.SplitAfter(s, "\n")
		for _, line := range lines {
			assetContent = assetContent + line
			if strings.Contains(line, "\n") {
				var pubSubMessage pubsub.Message
				var err error
				dumpLineNumber++
				pubSubMessage.Data, err = getFeedMessageJSON(assetContent, startTime, ev)
				if err != nil {
					glo.LogWarning(ev, "getFeedMessageJSON( scanner.Text())", err.Error())
				} else {
					publishResult := topic.Publish(ctx, &pubSubMessage)
					waitGroup.Add(1)
					go gps.GetPublishCallResult(ctx,
						publishResult,
						&waitGroup,
						fmt.Sprintf("dumpline number %d", dumpLineNumber),
						&pubSubErrNumber,
						&pubSubMsgNumber,
						ev)
				}
				assetContent = ""
			}
		}
		offset = offset + storageRangeReaderLengthBytes
	}
	if !strings.Contains(err.Error(), "The requested range cannot be satisfied") {
		glo.LogWarning(ev, "storageObject.NewRangeReader", err.Error())
	}
	waitGroup.Wait()
	return dumpLineNumber, pubSubMsgNumber, pubSubErrNumber, time.Since(start)
}
