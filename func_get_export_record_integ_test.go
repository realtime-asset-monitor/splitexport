// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package splitexport

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegGetExportStepStack(t *testing.T) {
	testCases := []struct {
		name               string
		createExportName   string
		getExportName      string
		processed          bool
		maxRetry           int
		wantMsgContains    []string
		wantErrMsgContains string
	}{
		{
			name:             "get_export_stepstack_ok",
			createExportName: "test01",
			getExportName:    "test01/0",
			maxRetry:         10,
		},
		{
			name:             "get_child_export_stepstack_ok",
			createExportName: "test02/t2020-03-14T07-34-17-275598699Z_fld800047992631_iam_policy_7929110e74d513b78cb7b24b8f9cdc7f2bbe5f3f344eec5027a819de0f076013/cloudresourcemanager.googleapis.com/Folder/0.1645001436590350.2022-02-16T08_50_36Z.child0",
			getExportName:    "test02/t2020-03-14T07-34-17-275598699Z_fld800047992631_iam_policy_7929110e74d513b78cb7b24b8f9cdc7f2bbe5f3f344eec5027a819de0f076013/cloudresourcemanager.googleapis.com/Folder/0.1645001436590350.2022-02-16T08_50_36Z.child0",
			maxRetry:         10,
		},
		{
			name:             "not_found",
			createExportName: "a",
			getExportName:    "b",
			maxRetry:         2,
			wantMsgContains: []string{
				"INFO",
				"code = NotFound desc",
			},
			wantErrMsgContains: "doc not found in firestore",
		},
	}
	ctx := context.Background()

	exportCollectionName := "TestIntegGetExportStepStack"
	var ev glo.EntryValues
	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "test"
	ev.Step.StepID = "0123456789"

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			err := gfs.RecordExportWithRetry(ctx,
				global.firestoreClient,
				exportCollectionName,
				tc.createExportName,
				tc.processed,
				10,
				ev)
			if err != nil {
				t.Error(err)
			}

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			exportRecord, err := getExportRecord(ctx,
				tc.getExportName,
				global.firestoreClient,
				exportCollectionName,
				tc.maxRetry,
				ev)
			msgString := buffer.String()
			// t.Logf("%v", msgString)

			for _, wantMsg := range tc.wantMsgContains {
				if !strings.Contains(msgString, wantMsg) {
					t.Errorf("want msg to contains: %s got %s", tc.wantMsgContains, msgString)
				}
			}
			if err != nil {
				if tc.wantErrMsgContains == "" {
					t.Errorf("Want NO error and got %v", err)
				} else {
					if !strings.Contains(err.Error(), tc.wantErrMsgContains) {
						t.Errorf("want err msg contains %s and got %s", tc.wantErrMsgContains, err.Error())
					}
				}
			} else {
				if tc.wantErrMsgContains != "" {
					t.Errorf("want err msg contains %s and got no error", tc.wantErrMsgContains)
				} else {
					if !reflect.DeepEqual(ev.StepStack, exportRecord.StepStack) {
						t.Errorf("retrieved step stack does on equal the original")
					}
				}
			}
		})
	}
	err := gfs.DeleteCollection(ctx,
		global.firestoreClient,
		global.firestoreClient.Collection(exportCollectionName),
		100)
	if err != nil {
		log.Fatalln(err)
	}
}
